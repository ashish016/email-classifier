from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class Credentials(models.Model):
    auth_user = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
    email = models.CharField(max_length=30, primary_key=True, null=False)
    password = models.CharField(max_length=30)
    protocol = models.CharField(max_length=100, default='')
    port = models.IntegerField(max_length=4, default=-1)
