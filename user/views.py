from django.shortcuts import render

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth.models import User
from rest_framework import status
from .models import Credentials
import imaplib
from emailapp.views import getMails

SMTP_SERVER = "imap.gmail.com"


# Create your views here.

class ApplicationUser(APIView):
    def post(self, request):
        try:
            email = request.data['email']
            password = request.data['password']
            first_name = request.data['first_name']
            last_name = request.data['last_name']
            user = User(username=email, first_name=first_name, last_name=last_name)
            user.set_password(password)
            user.save()
            return Response({

                "Success": "User Created Successfully !!"
            }, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({

                "Failed": str(e)
            }, status=status.HTTP_400_BAD_REQUEST)


class RegisterValidEmail(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):

        try:
            email = request.data['email']
            password = request.data['password']

            imap = imaplib.IMAP4_SSL(SMTP_SERVER)
            imap.login(email, password)

            credentials = Credentials(email=email, password=password, auth_user_id=request.user.id)
            credentials.save()
            getMails(email, password, True)
            return Response(
                {"Success": "Email Configured"}
            )
        except Exception as e:
            print(str(e))
            return Response(
                {"Success": "Email Failed to Configure"}
            )
