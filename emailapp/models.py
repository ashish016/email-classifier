from django.db import models
from django_mysql.models import ListTextField
import datetime
from django.contrib.auth.models import User


# Create your models here.

class Category(models.Model):
    auth_user = models.ForeignKey(User, on_delete=models.CASCADE, null=False, default=-1)
    id = models.AutoField(primary_key=True, null=False)
    name = models.CharField(unique=True, null=False, max_length=250)


class EmailData(models.Model):
    category_id = models.ForeignKey(Category, on_delete=models.CASCADE, null=True)
    subject = models.TextField(max_length=500)
    sent_from = models.CharField(max_length=200)
    datetime = models.DateTimeField(null=False, default=datetime.datetime.now())
    body = models.TextField()
    email_id = models.TextField(max_length=100, default='xyz@gmail.com')
    attachment = ListTextField(
        base_field=models.CharField(max_length=200),
        blank=True,
        size=10
    )
