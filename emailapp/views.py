from django.shortcuts import render

# Create your views here.
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
import poplib
import email
import logging
import os
from .models import *
from user.models import *
import datetime


def clean(text):
    return "".join(c if c.isalnum() else "_" for c in text)


def separate_date_fields(date):
    date = date.split('-')
    year = int(date[0])
    month = int(date[1])
    date = date[2].split(' ')
    day = int(date[0])
    hr, min, sec = date[1].split(':')
    return year, month, day, int(hr), int(min), int(sec)


def get_date_month_year_hr_min_sec(date):
    if '+' in date:
        date = date.split('+')
        return separate_date_fields(date[0])
    else:
        return separate_date_fields(date)


def should_save(last_item_from_db, current_item):
    if last_item_from_db == 0:
        return True
    else:
        current_item = getValidDate(current_item)
        current_item_year, current_item_month, current_item_day, current_item_hr, current_item_min, current_item_sec = get_date_month_year_hr_min_sec(
            current_item)
        last_item_from_db_year, last_item_from_db_month, last_item_from_db_day, last_item_from_db_hr, last_item_from_db_min, last_item_from_db_sec = get_date_month_year_hr_min_sec(
            str(last_item_from_db))

        if current_item_year > last_item_from_db_year:
            return True
        elif current_item_year >= last_item_from_db_year and current_item_month > last_item_from_db_month:
            return True
        elif current_item_year >= last_item_from_db_year and current_item_month >= last_item_from_db_month and current_item_day > last_item_from_db_day:
            return True
        elif current_item_year >= last_item_from_db_year and current_item_month >= last_item_from_db_month and current_item_day >= last_item_from_db_day and current_item_hr > last_item_from_db_hr:
            return True
        elif current_item_year >= last_item_from_db_year and current_item_month >= last_item_from_db_month and current_item_day >= last_item_from_db_day and current_item_hr >= last_item_from_db_hr and current_item_min > last_item_from_db_min:
            return True
        elif current_item_year >= last_item_from_db_year and current_item_month >= last_item_from_db_month and current_item_day >= last_item_from_db_day and current_item_hr >= last_item_from_db_hr and current_item_min >= last_item_from_db_min and current_item_sec > last_item_from_db_sec:
            return True
        else:
            return False


def getValidDate(given_date):
    day_list = {
        'mon': 1,
        'tue': 2,
        'wed': 3,
        'thr': 4,
        'fri': 5,
        'sat': 6,
        'sun': 7,

    }
    month_list = {
        'jan': 1,
        'feb': 2,
        'mar': 3,
        'apr': 4,
        'may': 5,
        'jun': 6,
        'jul': 7,
        'aug': 8,
        'sep': 9,
        'oct': 10,
        'nov': 11,
        'dec': 12
    }
    date = given_date
    day, rest = date.split(',')
    date = rest.split(' ')

    hr, min, sec = date[4].split(':')
    datetimeObj = str(date[3]) + "-" + str(month_list[date[2].lower()]) + "-" + str(date[1]) + " " + str(
        hr) + ":" + str(min) + ":" + str(sec)
    return datetimeObj


def getMails(user, password, run_reverse=False):
    SERVER = "pop.gmail.com"
    USER = user
    PASSWORD = password

    # connect to server
    logging.debug('connecting to ' + SERVER)
    server = poplib.POP3_SSL(SERVER)

    # login
    logging.debug('logging in')
    server.user(USER)
    server.pass_(PASSWORD)

    # list items on server
    logging.debug('listing emails')
    resp, items, octets = server.list()
    mail_from = ''
    subject = ''
    date = ''
    mail_body = ''

    if run_reverse:
        for i in range(0, len(items)):
            singleitem = str(items[i].decode("utf-8"))
            id, size = str.split(singleitem)
            resp, text, octets = server.retr(id)

            alltext = text
            mytext = bytes('', 'utf-8')
            for e in text:
                mytext = mytext + (e + bytes("\n", 'utf-8'))

            message = email.message_from_bytes(mytext)

            print('From: ', message['From']),
            mail_from = message['From']
            print('Subject: ', message['Subject']),
            subject = message['Subject']
            print('Date:', message['Date']),
            date = message['Date']
            msg = message

            if msg.is_multipart():
                for part in msg.walk():
                    # extract content type of email
                    content_type = part.get_content_type()
                    content_disposition = str(part.get("Content-Disposition"))
                    try:
                        # get the email body
                        body = part.get_payload(decode=True).decode()
                        if content_type == 'text/html':
                            print('Html Body: ', body)
                            mail_body = body
                        # print('Body :', body)
                    except:
                        pass
                    if content_type == "text/plain" and "attachment" not in content_disposition:
                        # print text/plain emails and skip attachments
                        # print(body)
                        pass
                    elif "attachment" in content_disposition:
                        # download attachment
                        filename = part.get_filename()
                        if filename:
                            folder_name = clean(msg['Subject'])
                            if not os.path.isdir(folder_name):
                                # make a folder for this email (named after the subject)
                                os.mkdir(folder_name)
                            filepath = os.path.join(folder_name, filename)
                            # download attachment and save it
                            open(filepath, "wb").write(part.get_payload(decode=True))
            else:
                # extract content type of email
                content_type = msg.get_content_type()
                # get the email body
                body = msg.get_payload(decode=True).decode()
                # if content_type == "text/plain":
                # print only text email parts
                # print('Text Body: ', body)
                if content_type == 'text/html':
                    print('Html Body: ', body)
                    mail_body = body

            try:
                email_date = EmailData(subject=subject, sent_from=mail_from, datetime=getValidDate(date),
                                       email_id=user,
                                       body='',
                                       attachment='')
                email_date.save()
            except Exception as e:
                print(str(e))

    else:

        for i in range(len(items) - 1, 0, -1):
            singleitem = str(items[i].decode("utf-8"))
            id, size = str.split(singleitem)
            resp, text, octets = server.retr(id)

            alltext = text
            mytext = bytes('', 'utf-8')
            for e in text:
                mytext = mytext + (e + bytes("\n", 'utf-8'))

            message = email.message_from_bytes(mytext)

            print('From: ', message['From']),
            mail_from = message['From']
            print('Subject: ', message['Subject']),
            subject = message['Subject']
            print('Date:', message['Date']),
            date = message['Date']
            get_last_record = EmailData.objects.last()
            last_record_time = ''
            try:
                last_record_time = get_last_record.datetime
            except:
                last_record_time = 0
            if should_save(last_record_time, date):
                # print('Body: ', message.get_payload()[1].get_payload())
                msg = message

                if msg.is_multipart():
                    for part in msg.walk():
                        # extract content type of email
                        content_type = part.get_content_type()
                        content_disposition = str(part.get("Content-Disposition"))
                        try:
                            # get the email body
                            body = part.get_payload(decode=True).decode()
                            if content_type == 'text/html':
                                print('Html Body: ', body)
                                mail_body = body
                            # print('Body :', body)
                        except:
                            pass
                        if content_type == "text/plain" and "attachment" not in content_disposition:
                            # print text/plain emails and skip attachments
                            # print(body)
                            pass
                        elif "attachment" in content_disposition:
                            # download attachment
                            filename = part.get_filename()
                            if filename:
                                folder_name = clean(msg['Subject'])
                                if not os.path.isdir(folder_name):
                                    # make a folder for this email (named after the subject)
                                    os.mkdir(folder_name)
                                filepath = os.path.join(folder_name, filename)
                                # download attachment and save it
                                open(filepath, "wb").write(part.get_payload(decode=True))
                else:
                    # extract content type of email
                    content_type = msg.get_content_type()
                    # get the email body
                    body = msg.get_payload(decode=True).decode()
                    # if content_type == "text/plain":
                    # print only text email parts
                    # print('Text Body: ', body)
                    if content_type == 'text/html':
                        print('Html Body: ', body)
                        mail_body = body

                try:
                    email_date = EmailData(subject=subject, sent_from=mail_from, datetime=getValidDate(date),
                                           email_id=user,
                                           body='',
                                           attachment='')
                    email_date.save()
                except Exception as e:
                    print(str(e))
            else:
                break


class SaveEmails(APIView):
    pass


class GetEmailsOnDemand(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        try:
            get_user_credentials = Credentials.objects.get(auth_user=request.user)
            getMails(get_user_credentials.email, get_user_credentials.password)
            return Response({
                "Success": "Mails Pulled again"
            })
        except Exception as e:
            return Response({
                "status": "Failed",
                "message": str(e)
            })


class DemoView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        print(request.user)
        return Response(
            {"Success": "You are Authenticated"}
        )


class CategoryView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        try:

            category_name = request.data['category_name']
            category = Category(name=category_name, auth_user=request.user)
            category.save()
            return Response(
                {"Success": "Category Created !!"}
            )
        except Exception as e:
            if 'Duplicate' in str(e):
                return Response({
                    "status": "Failed",
                    "message": "Duplicate Entry"
                })
            else:
                return Response({
                    "status": "Failed",
                    "message": str(e)
                })

    def get(self, request):
        try:
            user_id = request.user.id
            category_list = Category.objects.filter(auth_user=user_id).values('name', 'id')

            return Response({
                "status": "Success",
                "data": category_list
            })
        except Exception as e:
            return Response({
                "status": "Failed",
                "message": str(e)
            })


class GetMailsByCategory(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        try:
            category = kwargs['category_name']
            get_category_id = Category.objects.get(auth_user=request.user, name=category)
            print('get_category_id', get_category_id.id)
            get_category_mails = EmailData.objects.filter(category_id=get_category_id.id).order_by('-datetime').values(
                "id", "subject", "sent_from",
                "datetime")

            return Response({
                "status": "success",
                "date": get_category_mails
            })
        except Exception as e:
            return Response({
                "status": "Failed",
                "message": str(e)
            })


class GetUnCategorizedMailsAPI(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        try:
            get_uncategorized_mails = EmailData.objects.filter(category_id__isnull=True).order_by('-datetime').values(
                "id", "subject", "sent_from",
                "datetime")
            return Response({
                "status": "success",
                "date": get_uncategorized_mails
            })
        except Exception as e:
            return Response({
                "status": "Failed",
                "message": str(e)
            })


class GetEmailDataById(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        try:
            mail_id = kwargs['mail_id']
            get_mail_data = EmailData.objects.get(id=mail_id)
            mail_obj = {}
            mail_obj['subject'] = get_mail_data.subject
            mail_obj['from'] = get_mail_data.sent_from
            mail_obj['datetime'] = get_mail_data.datetime
            mail_obj['body'] = get_mail_data.body
            mail_obj['attchment'] = get_mail_data.attachment

            return Response({
                "status": "success",
                "data": mail_obj
            })
        except Exception as e:
            return Response({
                "status": "Failed",
                "message": str(e)
            })


class EmailDataCategozieAPI(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        try:
            category_id = request.data['category_id']
            email_id = request.data['email_id']
            email_obj = EmailData.objects.get(id=email_id)
            email_obj.category_id = Category.objects.get(id=category_id)
            email_obj.save(force_update=True)
            return Response({
                "status": "success",
                "message": "Email is assigned to a category"
            })
        except Exception as e:
            return Response({
                "status": "Failed",
                "message": str(e)
            })

    def put(self, request):
        try:
            category_id = request.data['category_id']
            email_id = request.data['email_id']
            email_obj = EmailData.objects.get(id=email_id)
            email_obj.category_id = Category.objects.get(id=category_id)
            email_obj.save(force_update=True)
            return Response({
                "status": "success",
                "message": "Email is now assigned to the new category"
            })
        except Exception as e:
            return Response({
                "status": "Failed",
                "message": str(e)
            })

    def delete(self, request):
        try:
            email_id = request.data['email_id']
            email_obj = EmailData.objects.get(id=email_id)
            email_obj.category_id = None
            email_obj.save(force_update=True)
            return Response({
                "status": "success",
                "message": "Email is removed from category the category"
            })
        except Exception as e:
            return Response({
                "status": "Failed",
                "message": str(e)
            })
