from celery import shared_task
from django.core.mail import send_mail
from time import sleep
from user.models import *
from .views import getMails


@shared_task
def send_email_task(email=None):
    if email is not None:
        send_mail('Congrats',
                  'Welcome To our Organization',
                  'No-reply.com',
                  [email],
                  )
        return 'sent'
    else:
        return None


@shared_task
def send_email_to_admin_task(new_user_email=None):
    if new_user_email is not None:
        send_mail('A new user has registered',
                  f'A user with this ${new_user_email} has Registered',
                  'No-reply.com',
                  ['vishaldash19@gmail.com'],
                  )
        return 'sent'
    else:
        return 'None'


@shared_task
def fetch_mail():
    user_cred = Credentials.objects.all()
    for user in user_cred:
        getMails(user.email, user.password)
