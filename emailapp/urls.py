from django.contrib import admin
from django.urls import path, include
from .views import *

urlpatterns = [
    path('demo/', DemoView.as_view()),
    path('category/', CategoryView.as_view()),
    path('category_mails/<str:category_name>/', GetMailsByCategory.as_view()),
    path('un-categorized_mails/', GetUnCategorizedMailsAPI.as_view()),
    path('get_on_demand/', GetEmailsOnDemand.as_view()),
    path('mail_details/<int:mail_id>/', GetEmailDataById.as_view()),
    path('catergorize_mail/', EmailDataCategozieAPI.as_view())
]
